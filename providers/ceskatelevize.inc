<?php
/**
 * @file
 *  This is an ceskatelevize provider include file for Embedded Media Video.
 *  Use this as a base for creating new provider files.
 *
 *  When using this, first make the following global replacements:
 *  * Replace CESKATELEVIZE with the name of your provider in all caps.
 *  * Replace ceskatelevize with the name of your provider in all lower case.
 *  * Replace Ceskatelevize with the name (to be translated) of your provider in
 *    uppercase.
 *
 *  You then need to go through each function and modify according to the
 *  requirements of your provider's API.
 */

/**
 *  This is the main URL for your provider.
 */
define('EMVIDEO_CESKATELEVIZE_MAIN_URL', 'http://www.ceskatelevize.cz/ivysilani/');

/**
 *  This is the URL of Ajax page, that loads movie data
 */
define('EMVIDEO_CESKATELEVIZE_AJAX_URL', 'http://www.ceskatelevize.cz/ajax/playlistURL.php');


/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_ceskatelevize_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_CESKATELEVIZE_DATA_VERSION', 1);

/**
 * Implementation of hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *  'provider' => The machine name of the provider. This must be the same as
 *    the base name of this filename, before the .inc extension.
 *  'name' => The translated name of the provider.
 *  'url' => The url to the main page for the provider.
 *  'settings_description' => A description of the provider that will be
 *    posted in the admin settings form.
 *  'supported_features' => An array of rows describing the state of certain
 *    supported features by the provider. These will be rendered in a table,
 *    with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *    the 'Feature' column will give the name of the feature, 'Supported'
 *    will be Yes or No, and 'Notes' will give an optional description or
 *    caveats to the feature.
 */
function emvideo_ceskatelevize_info() {
  $features = array(
    array(
      t('Full screen mode'),
      t('Yes'),
      t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
    );
  return array(
    'provider' => 'ceskatelevize',
    'name' => t('Ceska televize'),
    'url' => EMVIDEO_CESKATELEVIZE_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !ceskatelevize.', array('!ceskatelevize' => l(t('ceskatelevize.cz'), EMVIDEO_CESKATELEVIZE_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 * Implementation of hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['ceskatelevize'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_ceskatelevize_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the ceskatelevize provider.
  $form['ceskatelevize']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the ceskatelevize provider.
  $form['ceskatelevize']['player_options']['emvideo_ceskatelevize_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_ceskatelevize_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  return $form;
}

/**
 * Implementation of hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *  An optional string with the pasted URL or embed code.
 *  @return
 *  Either an array of regex expressions to be tested, or a string with the
 *  video code to be used. If the hook tests the code itself, it should
 *  return either the string of the video code (if matched), or an empty
 *  array. Otherwise, the calling function will handle testing the embed code
 *  against each regex string in the returned array.
 */
function emvideo_ceskatelevize_extract($parse = '') {
  return array(
    '@ceskatelevize\.cz/ivysilani/([^"\&]+)/@i',
  );
}

/**
 * Implementation of hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *  The string containing the video to watch.
 *  @return
 *  A string containing the URL to view the video at the original provider's site.
 */
function emvideo_ceskatelevize_embedded_link($video_code) {
  return EMVIDEO_CESKATELEVIZE_MAIN_URL . $video_code;
}

/**
 * The embedded flash displaying the ceskatelevize video.
 */
function theme_emvideo_ceskatelevize_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    $autoplay = $autoplay ? 'true' : 'false';
    $fullscreen = variable_get('emvideo_ceskatelevize_full_screen', 1) ? 'true' : 'false';
    $output = '<object width="'. $width .'" height="'. $height .'" type="application/x-shockwave-flash" data="http://img8.ceskatelevize.cz/libraries/player/flashPlayer.swf?version=1.415">';
    $output .= '<param name="bgcolor" value="#000000">';
    $output .= '<param name="align" value="middle" />';
    $output .= '<param name="quality" value="high" />';
    $output .= '<param name="allowScriptAccess" value="always" />';
    $output .= '<param name="allowFullScreen" value="'. $fullscreen .'" />';
    $output .= '<param name="pluginspage" value="http://www.adobe.com/go/getflashplayer" />';
    $output .= '<param name="flashvars" value="autoPlay='. $autoplay .'&amp;mute=false&amp;loop=false&amp;controlsHideable=true&amp;'
                .'playlistURL=' . _media_ceskatelevize_parse_playlist_url($item['value']) . '&amp;type=fpl&amp;'
                .'resizeControls=scale&amp;skinID=2&amp;uimode=controls&amp;expandable=false&amp;offsetX=5&amp;offsetY=5&amp;useClipBegin=true" />';
    $output .= '</object>';

    /*
     * // The old way
     * $output = '<br><a href="#selectBlockLinks" onclick="window.open(\'' . EMVIDEO_CESKATELEVIZE_MAIN_URL . $item['value'] . '#CTPlayer-1\', \'Video\',\'width=710,height=400,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no\');"><big>Přehrát video ≫</big></a>';
     */

  }
  return $output;
}

/**
 * Implementation of hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *  The video code for the video to embed.
 *  @param $width
 *  The width to display the video.
 *  @param $height
 *  The height to display the video.
 *  @param $field
 *  The field info from the requesting node.
 *  @param $item
 *  The actual content from the field.
 *  @return
 *  The html of the embedded video.
 */
function emvideo_ceskatelevize_video($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_ceskatelevize_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 * Implementation of hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *  The video code for the video to embed.
 *  @param $width
 *  The width to display the video.
 *  @param $height
 *  The height to display the video.
 *  @param $field
 *  The field info from the requesting node.
 *  @param $item
 *  The actual content from the field.
 *  @return
 *  The html of the embedded video.
 */
function emvideo_ceskatelevize_preview($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_ceskatelevize_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 * Implementation of hook_emfield_subtheme().
 *  This returns any theme functions defined by this provider.
 */
function emvideo_ceskatelevize_emfield_subtheme() {
  $themes = array(
    'emvideo_ceskatelevize_flash'  => array(
      'arguments' => array('item' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/ceskatelevize.inc',
      'path' => drupal_get_path('module', 'media_ceskatelevize'),
    )
  );
  return $themes;
}

/**
 *  Custom function to parse playlistID (the most important thing) from video_id
 *
 *  @param $video_id
 *  Part of url - ID of the video (stored in DB)
 *  @return
 *  ID of playlist or null, if not exists
 */
function _media_ceskatelevize_parse_playlist_url($video_id) {
  // Parse JS object with flash params
  foreach (@file(EMVIDEO_CESKATELEVIZE_MAIN_URL . $video_id) as $line) {
    if (strpos($line, 'callSOAP')) {
      $trim_start = strpos($line, '(') + 1;
      $trim_end = strpos($line, ')', $trim_start) - $trim_start;
      $options_raw = drupal_substr($line, $trim_start, $trim_end);
      break;
    }
  }

  // Page was not loaded
  if (empty($options_raw)) return NULL;

  // Prepare vars
  $options_json = json_decode($options_raw, TRUE);
  _media_ceskatelevize_remove_ads($options_json);
  $post_data = http_build_query($options_json);
  $post_data = str_replace("amp;", "", $post_data);
  $post_data_length = drupal_strlen($post_data);

  // Simulate AJAX request
  $playlist_url = file_get_contents(EMVIDEO_CESKATELEVIZE_AJAX_URL, FALSE,
    stream_context_create(
      array(
        'http' =>  array(
          'method' => 'POST',
          'header' => "Connection: close\r\nContent-Length: $post_data_length\r\n",
          'content' => $post_data
        )
      )
    )
  );

  if (!empty($playlist_url)) return $playlist_url;
  else return NULL;
}

/**
 *  Custom function to remove ads from video playlist
 *
 *  @param &$data
 *  Json array with playlist properties
 */
function _media_ceskatelevize_remove_ads(&$data) {
  $playlist = &$data['options']['playlistItems'];
  foreach ($playlist as $index => $item) {
    if ($item['Type'] == 'Ad') unset($playlist[$index]);
  }
  $playlist = array_values($playlist); // reset indexes
}