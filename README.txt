Media: Ceska Televize

This adds support for the Ceska Televize video sharing service, available at http://www.ceskatelevize.cz/ivysilani

To use this module, you'll first need to install Embedded Video Field, which is
packaged with Embedded Media Field (from http://drupal.org/project/emfield).

Set up a content type to use a Third Party Video field as you normally would with emfield.
Also ensure that you have enabled the new Ceska Televize provider from the Admin screen at /admin/content/emfield.
